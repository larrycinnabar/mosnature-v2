ymaps.ready(init);

var myMap;

function init () {
  $('.territory').height($(window).height() - $('header').height());

  myMap = new ymaps.Map('map', {
    center: [55.740436,37.553163],
    zoom: 11,
    controls: []
  });
  
  // Создание макета балуна
  var MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
    '<div class="popover popover-territory"><div class="arrow"></div>' +
      '$[[options.contentLayout]]' +
      '</div>', {
      /**
       * Строит экземпляр макета на основе шаблона и добавляет его в родительский HTML-элемент.
       * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#build
       * @function
       * @name build
       */
      build: function () {
        this.constructor.superclass.build.call(this);

        this._$element = $('.popover', this.getParentElement());

        this.applyElementOffset();
      },

      /**
       * Удаляет содержимое макета из DOM.
       * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#clear
       * @function
       * @name clear
       */
      clear: function () {
        this._$element.find('.close')
          .off('click');

        this.constructor.superclass.clear.call(this);
      },

      /**
       * Метод будет вызван системой шаблонов АПИ при изменении размеров вложенного макета.
       * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
       * @function
       * @name onSublayoutSizeChange
       */
      onSublayoutSizeChange: function () {
        MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

        if(!this._isElement(this._$element)) {
          return;
        }

        this.applyElementOffset();

        this.events.fire('shapechange');
      },

      /**
       * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
       * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
       * @function
       * @name applyElementOffset
       */
      applyElementOffset: function () {
        this._$element.css({
          left: 28,
          top: -35
        });
      },

      /**
       * Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
       * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
       * @function
       * @name onCloseClick
       */
      onCloseClick: function (e) {
        e.preventDefault();

        this.events.fire('userclose');
      },

      /**
       * Используется для автопозиционирования (balloonAutoPan).
       * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ILayout.xml#getClientBounds
       * @function
       * @name getClientBounds
       * @returns {Number[][]} Координаты левого верхнего и правого нижнего углов шаблона относительно точки привязки.
       */
      getShape: function () {
        if(!this._isElement(this._$element)) {
          return MyBalloonLayout.superclass.getShape.call(this);
        }

        var position = this._$element.position();

        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
          [position.left, position.top], [
            position.left + this._$element[0].offsetWidth,
            position.top + this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight
          ]
        ]));
      },

      /**
       * Проверяем наличие элемента (в ИЕ и Опере его еще может не быть).
       * @function
       * @private
       * @name _isElement
       * @param {jQuery} [element] Элемент.
       * @returns {Boolean} Флаг наличия.
       */
      _isElement: function (element) {
        return element && element[0] && element.find('.arrow')[0];
      }
    });

  // Создание вложенного макета содержимого балуна.
  var MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
    '$[properties.balloonImg]' +
    '<h3>$[properties.balloonHeader]</h3>' +
    '<div class="popover-text">$[properties.balloonContent]</div>' +
    '$[properties.balloonLink]'
  );

  //var oGroup = new ymaps.GeoObjectCollection({});
  // Загрузка объектов на карту
  var allObj = new Array();
  //var allObjByO = new Array();
  //var allObjByT = new Array();
  $(".terr-loader").show();

  $.ajax({
    url: "../getlist.json",  
    dataType: "json",
    success: function(msg){

      $(msg).each(function(i, item){
        if(item.t == "pl"){
          //console.log(item.g);
          //если полигон

          var obj = new ymaps.Polygon(item.g, {
               balloonImg: item.p ? '<div class="popover-img"><img src="'+item.p+'" /></div>' : '',
               balloonHeader: item.n,
               hintContent: item.n,
               balloonContent: item.d,
               balloonLink: item.l ? '<div class="popover-link"><a href="'+item.l+'" class="btn">Подробнее &rarr;</a></div>' : '',
           }, {
              strokeColor: '#528c05',
              fillColor: '#8cc63e73',
              strokeWidth: 2,
              draggable: false,
              balloonLayout: MyBalloonLayout,
              balloonContentLayout: MyBalloonContentLayout,
              balloonPanelMaxMapArea: 0,
              balloonShadow: false,
              iconLayout: 'default#image',
              balloonAutoPan: true,
              oopt: item.o, 
              cat: item.c, 
          });
//          if(!item.o) item.o = -1;
//          if(!allObjByO[item.o]) allObjByO[item.o] = new ymaps.GeoObjectCollection({});
          allObj.push(obj);
          //allObjByO[item.o].add(obj);
          //oGroup.add(obj);
          //myMap.geoObjects.add(obj);
          //console.log(obj);
        }
        if(item.t == "pt"){
          //если точка
          if(item.g[0] =="" && item.a){
            var myGeocoder = ymaps.geocode(item.a);
            myGeocoder.then(
              function (res) {
                var coords = res.geoObjects.get(0).geometry.getCoordinates();
              },
              function (err) {
                console.log('no coords');
              });
          }else{
            var coords = item.g;
          }
          //console.log(coords);
          //если есть координата
          if ( !item.hasOwnProperty('icon') ) item.icon = '1'; // по умолчанию
          if(coords){
            obj = new ymaps.Placemark(coords, {
                 balloonImg: item.p ? '<div class="popover-img"><img src="'+item.p+'" /></div>' : '',
                 balloonHeader: item.n,
                 hintContent: item.n,
                 balloonContent: item.d,
                 balloonLink: item.l ? '<div class="popover-link"><a href="'+item.l+'" class="btn">Подробнее &rarr;</a></div>' : '',
            }, {
              balloonLayout: MyBalloonLayout,
              balloonContentLayout: MyBalloonContentLayout,
              balloonPanelMaxMapArea: 0,
              balloonShadow: false,
              hideIconOnBalloonOpen: false,
              iconLayout: 'default#image',
              iconImageHref: 'images/map-icon-' + item.icon + '-normal.png',
              iconImageSize: [39, 39],
              iconImageOffset: [-19, -19]
              oopt: item.o, 
              cat: item.c, 
            });
            allObj.push(obj);
            //oGroup.add(obj);
          }
        }
      }); 
//      myMap.geoObjects.add(oGroup);
      $(".terr-loader").hide();
//      myMap.setBounds(oGroup.getBounds(),{checkZoomRange:true,duration:500});
      GQ = ymaps.geoQuery(allObj).addToMap(myMap); // создадим для фильтрации из всех объектов
      myMap.setBounds(GQ.getBounds(),{checkZoomRange:true,duration:500});
    },
   });  

  //обработчики фильтров
  function mapFilter(){
    var oopt = $(".form-select select").val();
    var shownObjects,
        byOopt = new ymaps.GeoQueryResult(),
        byCategory = new ymaps.GeoQueryResult();
            
    if(oopt){
      byOopt = GQ.search('options.oopt = "'+oopt+'"')
                .add(byOopt);
                //.add(myObjects.search('options.preset = "twirl#yellowIcon"'))
                console.log('searching');
    }else{
      byOopt = GQ.search('options.balloonShadow = false')
                .add(byOopt);
    }

    //обойдем список активных категорий
    var active_cats = new Array;
    var total_active_cats = 0;
    
    $(".territory-filter-group .territory-filter-item").each(function(){
      var v = $(this).attr("rel");
      var a = $(this).hasClass("checked");
      if(a){
          total_active_cats++;
          byCategory = GQ.search('options.cat = "'+v+'"').add(byCategory);   
      }
    });
    //добавим отедльно территории
    byCategory = GQ.search('options.cat = "100000"').add(byCategory);   
    
    
    console.log(byCategory.getLength());
    console.log(byOopt.getLength());
    shownObjects = byOopt.intersect(byCategory).addToMap(myMap); //.intersect(byShape)
    // Объекты, которые не попали в выборку, нужно убрать с карты.
    GQ.remove(shownObjects).removeFromMap(myMap);
    if(shownObjects.getLength() == 0 && !oopt && !total_active_cats){
      GQ.addToMap(myMap);
    }
  } 
   
  $(".form-select select").change(mapFilter);
  $(".territory-filter-group .territory-filter-item").click(mapFilter);


}